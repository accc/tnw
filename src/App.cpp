/*

    App.cpp
    App class definition

	Alexi Chiotis

*/

#include "App.h"
#include "Game.h"
#include "Console.h"
#include <sstream>
#include <iostream>
#include <iomanip>

#include "Random.h"

using namespace std;

const char *App::kCityFilename = "cities.txt";

App::App()
{
    boardWidth = 0;
    boardHeight = 0;
    nCities = 0;
	testMode = false;
	isDone = false;
	initialOutput = true;
}

void App::displayTitle()
{
	Console::clear();
	stringstream titless;
    titless << " Thermonuclear War" << endl
			<< " Alexi Chiotis - 2012" << endl << endl;

	if (initialOutput) {
		Console::simulateTyping(titless.str());
		initialOutput = false;
	}
	else
		cout << titless.str();
}

void App::displayHomeScreen()
{
    while (true) {
		displayTitle();
		stringstream homess;
        homess << "   (N)ew Game";
		
		// if any of these values have not been entered
		if (boardWidth * boardHeight * nCities)
			homess << " - Board: " << boardWidth << "x" << boardHeight << endl
				   << "              - Player cities: " << nCities;

		homess << endl << endl
				<< "   (E)dit Game Board Details" << endl << endl
				<< "   (T)est Mode " << (testMode ? "ON - reveal cities" : "OFF") << endl
				<< "   (I)nstructions" << endl
				<< "   (Q)uit" << endl << endl
				<< "> ";

		cout << homess.str();

        string selection = "";
        Console::getValue(selection);
		Console::strToLower(selection);

		if (selection == "n") {
            return;
		}
		else if (selection == "e") {
			editGameDetails();
            continue;
		}
		else if (selection == "t") {
			testMode = !testMode;
			continue;
		} 
		else if (selection == "i") {
			displayInstructions();
            cin.get();
            continue;
		}
		else if (selection == "q") {
			exit(0);
            return;
		}
		else {
            continue;
        }
    }
}

void App::displayInstructions()
{
    system("more Instructions.txt");
}

void App::editGameDetails()
{
	// tnwApp remembers the above values for user convenience
	boardWidth = boardHeight = nCities = 0;
    
	// instantiate a temporary Game object just to access its method
	Game tmp(*this);
	tmp.promptGameDetails();
}

void App::run()
{
    while (!isDone) {
        displayHomeScreen();

        Game theGame(*this);
        theGame.init();
        theGame.run();
        theGame.cleanup();
    }
}

void App::setArgs(int argc, char *argv[])
{
    --argc; // Don't include the app path
    if (argc > 3) argc = 3; // ignore additional arguments, limit possible cases to 1-3

    stringstream tempss;    // for char * to int type conversion
    switch (argc)
    {
        case 3: // each case performs conversion from char string to unsigned
            tempss << argv[3];
            tempss >> nCities;
            tempss.clear();
        case 2:
            tempss << argv[2];
            tempss >> boardHeight;
            tempss.clear();
        case 1:
            tempss << argv[1];
            tempss >> boardWidth;
            tempss.clear();

            break;
        default:
            break;
    }
}

void App::createCityListFromFile()
{
	cout << "Loading city data... ";
    //  loads City vector with data from tab separated cities.txt file
	ifstream fileIn(kCityFilename);
    if (!fileIn) {
        cerr << "error opening file: " << kCityFilename << endl << endl
			 << "Please ensure file is in same directory as the app. Exiting." << endl;
//		cin.get();
        exit(1);
    }

	// WISH Error checking
    string cityName;
    while (getline(fileIn, cityName)) {
        City c(cityName, 0);
        cities.push_back(c);
//      cout << c.stats() << endl;
    }
	cout << "done." << endl;
}

void App::init()
{
	Game tmp(*this);
	Random::init();
    createCityListFromFile();
	
	if (argc > 0)	// validate board args if passed on cmd line
		tmp.validateBoardArgs();
}

int main(int argc, char *argv[])
{
    App tnwApp;

    tnwApp.setArgs(argc, argv);	// get cmd line options
    tnwApp.init();

    while (!tnwApp.isDone)
        tnwApp.run();

    return 0;
}


