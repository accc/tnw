#ifndef RANDOM_H
    #define RANDOM_H

#include <time.h>
#include <cstdlib>

namespace Random
{
    void init();
    int generateRandomInt(int inRandMin, int inRandMax);
}
#endif
