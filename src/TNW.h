#ifndef TNW_H
    #define TNW_H

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <locale> // for is alpha

const unsigned kGridWidthMax = 16;
const unsigned kGridWidthMin = 6;

const unsigned kGridHeightMax = 10;
const unsigned kGridHeightMin = 5;

const unsigned kNumCitiesPerPlayerMax = 15;
const unsigned kNumCitiesPerPlayerMin = 1;
const unsigned kNumMissilesAdditionalToCities = 5;
const unsigned kDefaultCityDensity = kGridWidthMax * kGridHeightMax / (kNumCitiesPerPlayerMax * 2) - 1; // elements per city - 150 / 30 = 5 (4:1)

//const unsigned kLineCharsMax = 4096;
//const unsigned kNumPlayers = 2;
const unsigned kTurnsPerSpyMode = 5;
const std::string citiesFile = "cities.txt";

#include "City.h"
#include "Game.h"
#include "Player.h"
#include "Grid.h"
#include "Console.h"
//#include "Tokenizer.h"


#endif
