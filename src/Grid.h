#ifndef GRID_H
    #define GRID_H

#include "Player.h"
#include "City.h"

#include <iostream>
#include <string>

class Grid;	// forward declarations required because these classes refer to each other
class App;

// Each location on the grid
class GridElement
{
private:
    City* mCity;
    Player* mOwner;
    bool fNuked;
    bool fSpied;

public:
    GridElement();
    void setCity(City* inCity, Player* inOwner);

	City* getCity();	// return a pointer to check for NULL value
    Player* getOwner();

    #ifdef _WIN32		// could have special characters for Win version
    enum displayChars { kHidden = '-',
                        kNukedCity = '*', kNukedVacant = 'X',
                        kSpiedCity = 'C', kSpiedVacant = '^',
						kBoardDivider = '|', kBoardSpace = ' ',
						kTestCity = 'c', kTestVacant = '-' };
    #else
    enum displayChars { kHidden = '-',
                        kNukedCity = '*', kNukedVacant = 'X',
                        kSpiedCity = 'C', kSpiedVacant = '^',
						kBoardDivider = '|', kBoardSpace = ' ',
						kTestCity = 'c', kTestVacant = '-' };
    #endif

    void nuke();
	bool isNuked();

	void spy();
    void unSpy();
    bool isSpied();
    
	char display(bool testMode);
    std::string dump();
};

class Game;

// entire playable game grid
class Grid
{
public:
    Grid(unsigned int inWidth, unsigned int inHeight, Game &inGame);
    ~Grid();

    Game &theGame;  // used to get current player's region
    
	void setWidth(unsigned int inWidth);
    unsigned getWidth();
	void setHeight(unsigned int inHeight);
	unsigned getHeight();

    void setCityAt(unsigned int inX, unsigned int inY, City* inCity, Player* inOwner);

    unsigned int getNumElements();

    GridElement& getElementAt(unsigned int inX, unsigned int inY);
    City* getCityAt(unsigned int inX, unsigned int inY);

	void nukeAt(unsigned int inX, unsigned int inY);
    bool isNukedAt(unsigned inX, unsigned inY);

    void setSpyRegion(unsigned inX, unsigned inY);
    void clearSpyRegion();

    std::string dumpAt(unsigned int inX, unsigned int inY);

	#ifdef _WIN32		// could have special characters for Win version
    enum displayChars { kBoardDivider = '|', kBoardSpace = ' ' };
    #else
    enum displayChars { kBoardDivider = '|', kBoardSpace = ' ' };
    #endif

    void display();
    std::string buildxCoordStr();

private:
    GridElement* mBattleGrid;
    unsigned int mWidth, mHeight;
};

#endif