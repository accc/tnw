/* 

	Grid.Cpp
	Grid and Grid Element Implementation 
    
	Alexi Chiotis

*/

#include "TNW.h"

using namespace std;

class Game;
// WISH const input
Grid::Grid(unsigned int inWidth, unsigned int inHeight, Game &inGame)
: theGame(inGame)
{
    setWidth(inWidth);
    setHeight(inHeight);
    mBattleGrid = new GridElement[getNumElements()];
}

Grid::~Grid()
{
   delete mBattleGrid;
}

void Grid::setCityAt(unsigned int inX, unsigned int inY, City *inCity, Player* inOwner)
{
    getElementAt(inX, inY).setCity(inCity, inOwner);
}

void Grid::setWidth(unsigned int inWidth)
{
    mWidth = inWidth;
}

void Grid::setHeight(unsigned int inHeight)
{
    mHeight = inHeight;
}

unsigned int Grid::getWidth()
{
    return mWidth;
}

unsigned int Grid::getHeight()
{
    return mHeight;
}

unsigned int Grid::getNumElements()
{
    return getWidth() * getHeight();
}

GridElement& Grid::getElementAt(unsigned int inX, unsigned int inY)
{   // WISH: return check of reference value
	// should never get to this code
    // bounds checking
    if (inX >= getWidth() || inY >= getHeight()) {
        cerr << endl <<"Outside bounds! Attmempt at grid(" << inX << "," << inY << ")" << endl;
        cin.get();
        return *mBattleGrid;
    }
    else
        return *(mBattleGrid + inX + (getWidth() * inY));
}

City* Grid::getCityAt(unsigned int inX, unsigned int inY)
{
    return getElementAt(inX, inY).getCity();
}

void Grid::nukeAt(unsigned int inX, unsigned int inY)
{
    GridElement &g = getElementAt(inX, inY);
    g.nuke();

    theGame.currentPlayer().decMissiles();
    theGame.updateScreenWithInput();
        
	if (g.getOwner() != NULL) {	// player hit a city
		Console::simulateTyping("BOOM!!!\t\b\b\b\b\b\b\b\b\b");

		// Output message with nuked city stats
		string msg = "Nuked " + g.getOwner()->getName() + "'s " + g.getCity()->stats() + '\n';
		cout << msg;
        Console::delayms(1500);
        
		// Adjust all values of the attacker and target
        g.getOwner()->decMissiles();
		g.getOwner()->decHomeCitiesRemaining();

        theGame.currentPlayer().addToEnemyCasualties(g.getCity()->getPopulation());
        theGame.currentPlayer().addCityToDestroyedList(*(g.getCity()));
        theGame.currentPlayer().decEnemyCitiesRemaining();
    }
	else {
		Console::simulateTyping("Missed.\n");
	}
	Console::delayms(1000);
}

void Grid::setSpyRegion(unsigned centreX, unsigned centreY)
{
    unsigned boarder = 1;	// display 1 element around target

	// the range spy region will apply to
    int startX = centreX - boarder, endX = centreX + boarder + 1;
    int startY = centreY - boarder, endY = centreY + boarder + 1;

    for (int y = startY; y != endY; ++y)
    {
        for (int x = startX; x != endX; ++x)
        {
			// bounds checking
            if (x >= static_cast<int>(theGame.getCurrentPlayerLowX()) &&
                x <= static_cast<int>(theGame.getCurrentPlayerHighX())
                && y >= 0 && y <= static_cast<int>(getHeight()-1))
                getElementAt(x,y).spy();
        }
    }
}

void Grid::clearSpyRegion()
{
    // clear all spies for current player region
    for (unsigned y = 0; y != getHeight(); ++y)
    {
        for (unsigned x = theGame.getCurrentPlayerLowX(); x <= theGame.getCurrentPlayerHighX(); ++x)
        {
            getElementAt(x,y).unSpy();
        }
    }
}

string Grid::dumpAt(unsigned int inX, unsigned int inY)
{
    return getElementAt(inX, inY).dump();
}

string Grid::buildxCoordStr()
{
    unsigned lowX = theGame.getCurrentPlayerLowX();
    unsigned highX = theGame.getCurrentPlayerHighX();
    cout << endl;

    string xCoordStr;
    char xChar = 'A';

    // build the alphabetical x-co-ord string
    for (unsigned int x = 0; x < getWidth(); x++) {
        // determine players range
        if (x >= lowX && x <= highX) {
            xCoordStr += xChar;
            xCoordStr += ' ';
        }
        else {
            xCoordStr += "  ";
        }
        xChar++;
    }
    xCoordStr += '\n';
    return xCoordStr;
}

void Grid::display()	// display the Grid on screen
{
    string indent = "   ";
    string xCoordStr = indent + "  " + buildxCoordStr();
    cout << xCoordStr;	// display the x co-ordinate row

    // print the game board
    char paddingChar = kBoardSpace;
    for (unsigned int y = 0; y < getHeight(); y++)
    {
        cout << indent << y << char(kBoardDivider);
        for (unsigned int x = 0; x < getWidth(); x++) {

            if (x == getWidth() - 1 || !((x+1) % (getWidth() / theGame.kNumPlayers)))	// position the divider
				paddingChar = kBoardDivider;
            else
                paddingChar = kBoardSpace;

			cout << getElementAt(x, y).display(theGame.tnwApp.testMode) << paddingChar;	// display the actual element
        }
        cout << y << endl;
    }

    cout << xCoordStr << endl;	// print the x co-ords again on the bottom row
}

GridElement::GridElement()
{
    mCity = NULL;
    mOwner = NULL;
    fNuked = false;
    fSpied = false;
}

bool GridElement::isNuked()
{
    return fNuked;
}

// link an element to a city and an owner
void GridElement::setCity(City* inCity, Player* inOwner)
{
    mCity = inCity;
    mOwner = inOwner;
}

City* GridElement::getCity()
{
    return mCity;
}

Player* GridElement::getOwner()
{
    return mOwner;
}

void GridElement::nuke()
{
    fNuked = true;
}

void GridElement::spy()
{
    fSpied = true;
}

void GridElement::unSpy()
{
    fSpied = false;
}

bool GridElement::isSpied()
{
    return fSpied;
}

bool Grid::isNukedAt(unsigned inX, unsigned inY)
{
    return getElementAt(inX, inY).isNuked();
}

// display an individual element dependent on state
char GridElement::display(bool testMode)
{
    City* c = getCity();

    if (isNuked())
		return (c ? kNukedCity : kNukedVacant);
    else if (isSpied())
		return (c ? kSpiedCity : kSpiedVacant);
    else {
		if (testMode)
			return (c ? kTestCity : kTestVacant);
		else
			return kHidden;
	}
}

string GridElement::dump()	// debugging routine
{
    return "C: " + (getCity() ? getCity()->stats() : "Vacant") +
            (isNuked() ? "Nuked" : "OK") + '\n';
}
