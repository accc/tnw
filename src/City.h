#ifndef CITY_H
    #define CITY_H

#include "City.h"
#include <string>

class City
{
private:
	std::string name;
    unsigned long population;

public:
    City(std::string inName, unsigned long inPopulation);
	
	void setName(std::string inName);
    std::string getName();

    void setPopulation(unsigned long inPopulation);
    unsigned long getPopulation();
    
	std::string stats();
};

#endif
