/*

    Game.h
	Game class declarations

	Alexi Chiotis

*/

#ifndef GAME_H
    #define GAME_H

#include "Player.h"
#include "Grid.h"
#include "App.h"
#include <vector>
#include <string>

using namespace std;

class Game
{
public:
    Game(App &inApp);
    virtual ~Game();

    void init();
    void run();
    void cleanup();

    // Used by Grid to draw current player range
    unsigned getCurrentPlayerLowX();
    unsigned getCurrentPlayerHighX();
    
	Player& currentPlayer();
    void handleWarInput();	// used by Player class
    static unsigned const kTurnsPerSpyMode = 5;
    void setWarInputValid(bool inState);
    Grid *theBoard;
    
	static unsigned const kNumPlayers = 2;
    unsigned getCitiesPerPlayer();
    void setUserAbort(bool inState);
    bool isUserAbort();
    void promptGameDetails();
	void validateBoardArgs();
	void updateScreenWithInput();


	App &tnwApp;
	std::string dummyInput;		// used for keeping user's input when updating screen
	std::string postTurnMsg;

protected:
    static const string citiesFilename;

    static unsigned const kGridWidthMax = 16;
    static unsigned const kGridWidthMin = 6;

    static unsigned const kGridHeightMax = 10;
    static unsigned const kGridHeightMin = 5;

    static unsigned const kNumCitiesPerPlayerMax = 15;
    static unsigned const kNumCitiesPerPlayerMin = 1;
    static unsigned const kNumMissilesAdditionalToCities = 5;
    static unsigned const kDefaultCityDensity = kGridWidthMax * kGridHeightMax / (kNumCitiesPerPlayerMax * 2) - 1; // elements per city - 150 / 30 = 5 (4:1)

    Player *players[kNumPlayers];
    static unsigned currentPlayerIndex;

    unsigned boardWidth, boardHeight,
             citiesPerPlayer;

    vector<City> &cities;

    bool gameOver, inputValid, userAbort, invalidCoordCmd, firstTurn;
  	std::string inputString;
  	unsigned inputStringLength;

    void setBoardWidth(unsigned &inWidth);
    void setBoardHeight(unsigned &inHeight);
    void promptBoardDetails();
    void generateCityPopulations();
    void initRandomGenerator();
    int generateRandomInt(int inRandMin, int inRandMax);
    bool isGameOver();
    bool isGameRunning();
    void setGameRunning(bool inState);

    void createGameBoard();
    void createTwoPlayers();

    void setRandomStartingPlayer();
    void populateBoard();

    void preTurnUpdate();
    void postTurnUpdate();

	void updateScreenWithAlerts();
	string playerPrompt();
    void drawBoard();

    void setGameOver(bool inState);

    bool isWarInputValid();
    bool isInvalidCoordCmd();
    void setInvalidCoordCmd(bool inState);

    Player* currentPlayerPtr();
    Player* nextPlayerPtr();

    bool passToNextPlayer();
    void setCurrentPlayerIndex(unsigned inIndex);
    unsigned getCurrentPlayerIndex();
    bool isCurrentPlayerLast();

    void setCurrentPlayerToFirst();
    void setCurrentPlayerToLast();
    void setPlayerAttributes();

    void setCitiesPerPlayer(unsigned &inNumCitiesPerPlayer);
    unsigned boardWidthPerPlayer();

	void handleCoordCmd();
    void handleHelpCmd();
    void handleQuitCmd();
    void displayScores();

};


#endif
