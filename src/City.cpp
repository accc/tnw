/*

    City.cpp
    City class implementation

	Alexi Chiotis

*/
#include "City.h"
#include "Console.h"

using namespace std;

City::City(string inName, unsigned long inPopulation)
{
    setName(inName);
    setPopulation(inPopulation);
}

void City::setName(string inName)
{
	name = inName;
}

string City::getName()
{
    return name;
}

void City::setPopulation(unsigned long inPopulation)
{
    population = inPopulation;
}

unsigned long City::getPopulation()
{
    return population;
}

string City::stats()
{
    return getName() + " - pop. " + Console::toString(population);
}
