/*

    Player.h
    Player class definition

	Alexi Chiotis

*/

#ifndef PLAYER_H
    #define PLAYER_H

//#include "Game.h"
#include <string>
#include <sstream>
#include <vector>
#include "City.h"

class Game;

class Player
{
protected:
    Game &theGame;
    std::string name;

    std::vector<City> destroyedCities;

    unsigned homeCitiesRemaining, enemyCitiesRemaining, missilesRemaining;
    unsigned turn, spies;
    unsigned long enemyCasualtyTally;
    static unsigned nCpuPlayers;
    static unsigned nHumanPlayers;
    static unsigned nPlayersIn;
    bool out, prevTurnSpy;
    bool reportedOut;
// TODO Make as many methods as possible protected
public:
    Player(Game &inGame);// : theGame(inGame)

    virtual ~Player();
    std::string getName();
    void setName(std::string inName);
    std::ostringstream errorMsg;
    std::string alertMsg, postMsg;

    virtual void takeTurn() = 0;
    unsigned getTurn();
    void incrementTurn();
    unsigned getMissilesRemaining();
    void setMissilesRemaining(unsigned inMissilesRemaining);
    void decMissiles();
    void decHomeCitiesRemaining();
    void decEnemyCitiesRemaining();

    void setHomeCitiesRemaining(unsigned inCities);
    unsigned getHomeCitiesRemaining();
    void setEnemyCitiesRemaining(unsigned inCities);
    unsigned getEnemyCitiesRemaining();

    void addToEnemyCasualties(unsigned long inCasualties);
    unsigned long getTotalEnemyCasualties() const;
    unsigned getnPlayersIn();
	void decnPlayersIn();

	void setnPlayersIn(unsigned);
    void setReportedOut(bool inState);
    bool isReportedOut();
    std::string stats();
    std::string displayDestroyedCities();
    void addCityToDestroyedList(City &inCity);

/*
    // operator< must be implemented for std::sort
    // this will sort ascending

	bool operator< (const Player &rhs) const;

//*/
    void setOut();
    bool isOut();
    unsigned getSpies();

    void useSpy();
    void resetSpy();
    bool isPrevTurnSpy();
    virtual void gainSpy();
};

class HumanPlayer : public Player
{
public:
    HumanPlayer(Game &inGame);// : Player(inGame)
    ~HumanPlayer();

    virtual void takeTurn();
    void gainSpy();
};

class CpuPlayer : public Player
{
public:
    CpuPlayer(Game &inGame);// : Player(inGame)
    ~CpuPlayer();

    virtual void takeTurn();
};



#endif
