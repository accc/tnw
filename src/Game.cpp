/*

    Game.cpp
    Game class implementation

	Alexi Chiotis

*/

#include "App.h"
#include "Game.h"
#include "Console.h"
//#include "Tokenizer.h"
#include "Random.h"
#include <iomanip>

#include <cstdlib>

using namespace std;

unsigned Game::currentPlayerIndex = 0;

Game::Game(App &inApp) : tnwApp(inApp), cities(tnwApp.cities) {}

Game::~Game() {}

void Game::setGameOver(bool inState) { gameOver = inState; }
void Game::setUserAbort(bool inState) { userAbort = inState; }
bool Game::isUserAbort() { return userAbort; }

void Game::setWarInputValid(bool inState) { inputValid = inState; }
bool Game::isWarInputValid() { return inputValid; }
void Game::setInvalidCoordCmd(bool inState) { invalidCoordCmd = inState; }
bool Game::isInvalidCoordCmd() { return invalidCoordCmd; }

void Game::generateCityPopulations()
{
	random_shuffle(cities.begin(), cities.end());

    unsigned citiesOffset = cities.size() / kNumPlayers;
    for (unsigned i = 0; i < citiesOffset; ++i) {
		// set the populations between 2-30 million
		unsigned population = Random::generateRandomInt(2,30) * 1000000;

        // set the populations of the cities symmetrically for all players so that the game is fair
        for (unsigned currentPlayer = 0; currentPlayer < kNumPlayers; ++currentPlayer) {
            cities[currentPlayer*citiesOffset + i].setPopulation(population);
        }
    }

    for (unsigned i = 0; i < cities.size(); ++i) {
//        cout << cities[i].stats() << endl;
    }

    for (unsigned nCitiesToRemove = cities.size() % kNumPlayers; nCitiesToRemove != 0; nCitiesToRemove--)
        cities.pop_back();  // city list should be divisible by number of players
}

void Game::setBoardWidth(unsigned &inWidth)
{
    bool userRequestRandomWidth = false;

    // WISH fix board size random to allow for arbitrary N players
    if (inWidth == 0) {
        inWidth = Random::generateRandomInt(kGridWidthMin, kGridWidthMax);
        userRequestRandomWidth = true;
    }

    // Board width should be evenly divisible by number of Players
    if ((inWidth % kNumPlayers) != 0) {
        inWidth -= (inWidth % kNumPlayers);
        if (!userRequestRandomWidth) cout << "    Resizing board width for " << kNumPlayers << " players." << endl;
    }

    // bounds check the inWidth and height
    if (inWidth > kGridWidthMax) inWidth = kGridWidthMax;
    if (inWidth < kGridWidthMin) inWidth = kGridWidthMin;

    boardWidth = inWidth;
}

void Game::setBoardHeight(unsigned &inHeight)
{
    if (inHeight == 0) inHeight = Random::generateRandomInt(kGridHeightMin, kGridHeightMax);

    if (inHeight > kGridHeightMax) inHeight = kGridHeightMax;
    if (inHeight < kGridHeightMin) inHeight = kGridHeightMin;

    boardHeight = inHeight;
}

void Game::setCitiesPerPlayer(unsigned &inCitiesPerPlayer)
{
    unsigned elementsPerPlayer = boardHeight * boardWidth / kNumPlayers;

	// user requested random number of cities
    if (inCitiesPerPlayer == 0) inCitiesPerPlayer = Random::generateRandomInt(kNumCitiesPerPlayerMin, kNumCitiesPerPlayerMax);

    // ensure desired number of cities is within the range specs
    if (inCitiesPerPlayer > kNumCitiesPerPlayerMax) inCitiesPerPlayer = kNumCitiesPerPlayerMax;
    if (inCitiesPerPlayer < kNumCitiesPerPlayerMin) inCitiesPerPlayer = kNumCitiesPerPlayerMin;

    // ensure that there are not more cities than elements to hold them!
    if (inCitiesPerPlayer >= elementsPerPlayer) inCitiesPerPlayer = elementsPerPlayer - 1;

    citiesPerPlayer = inCitiesPerPlayer;
}

void Game::validateBoardArgs()
{
	setBoardWidth(tnwApp.boardWidth);
    setBoardHeight(tnwApp.boardHeight);
    setCitiesPerPlayer(tnwApp.nCities);
}

void Game::promptGameDetails()
{
    if (tnwApp.boardWidth == 0) {   // no previous value from cmdLine
        cout << "    Board width (" << kGridWidthMin << "-" << kGridWidthMax << ", 0 for random): ";
        Console::getValue(tnwApp.boardWidth);
    }
 
    if (tnwApp.boardHeight == 0) {
        cout << "    Board height (" << kGridHeightMin << "-" << kGridHeightMax << ", 0 for random): ";
        Console::getValue(tnwApp.boardHeight);
    }

    if (tnwApp.nCities == 0) {
        cout << "    Cities per player (" << kNumCitiesPerPlayerMin << "-"
			 << kNumCitiesPerPlayerMax <<", 0 for random): ";
        Console::getValue(tnwApp.nCities);
    }

	validateBoardArgs();
}

void Game::createGameBoard()
{
	// pass reference of the game to the board
	theBoard = new Grid(boardWidth, boardHeight, *this);
}

void Game::populateBoard()
{
	// Put some cities on the board
    cout << "Populating board... ";
    setCurrentPlayerToFirst();
    do {
        for (unsigned cityOffset = 0; cityOffset < getCitiesPerPlayer(); ) {

            // find a random spot on the board in the player's range
            unsigned x = Random::generateRandomInt(getCurrentPlayerLowX(), getCurrentPlayerHighX());
            unsigned y = Random::generateRandomInt(0, theBoard->getHeight()-1);

            if (theBoard->getCityAt(x,y) == NULL) {	// No city at this location of the board
                
				// offset the index using currentPlayerIndex and getNum..() 
				// so that we move along the vector in blocks for each player
                unsigned i = currentPlayerIndex * cities.size() / kNumPlayers + cityOffset;
                
				// set the element to an enemy's city in our shuffled list
                theBoard->setCityAt(x,y, &cities[i], nextPlayerPtr());
                ++cityOffset;
            }
        }
    } while (!passToNextPlayer());   // stop once back at first player
    cout << "done." << endl;
}

bool Game::isGameOver() { return gameOver; }

void Game::drawBoard()
{
	Console::clear();
	theBoard->display();
}

unsigned Game::getCurrentPlayerIndex() { return currentPlayerIndex; }

void Game::setCurrentPlayerIndex(unsigned inIndex) { currentPlayerIndex = inIndex; }

Player& Game::currentPlayer()
{
    return *players[getCurrentPlayerIndex()];
}

Player* Game::nextPlayerPtr()
{
    if ((getCurrentPlayerIndex() + 1) >= kNumPlayers) {
        return players[0];
    }
    else {
        return players[getCurrentPlayerIndex() + 1];
    }
}

void Game::setCurrentPlayerToFirst()
{
    setCurrentPlayerIndex(0);
}

void Game::setCurrentPlayerToLast()
{
    setCurrentPlayerIndex(kNumPlayers - 1);
}

unsigned Game::boardWidthPerPlayer()
{
    return theBoard->getWidth() / kNumPlayers;
}

unsigned Game::getCurrentPlayerLowX()
{
    return getCurrentPlayerIndex() * boardWidthPerPlayer();
}

unsigned Game::getCurrentPlayerHighX()
{
    return getCurrentPlayerLowX() + boardWidthPerPlayer() - 1;
}

bool Game::isCurrentPlayerLast()
{
    return (getCurrentPlayerIndex() == (kNumPlayers - 1));
}

//  Used for player set up and gameplay
bool Game::passToNextPlayer()
{
    // returns true if wrapped to first
    if (isCurrentPlayerLast()) {
        setCurrentPlayerToFirst();
        return true;
    } else {
        currentPlayerIndex++;
        return false;
    }
}

unsigned Game::getCitiesPerPlayer()
{
    return citiesPerPlayer;
}

void Game::handleCoordCmd()
{
    //locale loc; //!isalpha(inputString[0]) and !isdigit(inputString[1])
	// required for isdigit and isalpha method

	// input validation
    if ( !(inputStringLength == 3 || inputStringLength == 2) ||	// Co-ordinates should be 2 or 3 chars long
		!isalpha(inputString[0]) ||     // x co-ordinate should be alphabetical
        !isdigit(inputString[1]) ) {    // y co-ordinate should be a digit
        setInvalidCoordCmd(true);
        return;
    }

    char xAlpha = inputString[0];	// extract X co-ord char
    unsigned x = xAlpha - 'a';		// and convert to a number

    // Player within own board X-range?
    if (x < getCurrentPlayerLowX() || x > getCurrentPlayerHighX()) {
        currentPlayer().errorMsg << "x co-ordinate-range is " <<
        (char) (getCurrentPlayerLowX() + 'A') << "-" << (char) (getCurrentPlayerHighX() + 'A') << endl;
        return;
    }

	// Player within own board Y-range?
	unsigned y = inputString[1] - '0';     // convert char to integer y co-ordinate
    if (y >= theBoard->getHeight()) {
        currentPlayer().errorMsg << "y co-ordinate range is 0-" << (theBoard->getHeight() - 1) << endl;
        return;
    }

    if (inputStringLength == 2) {          // input is valid, attempt nuke
        if (theBoard->isNukedAt(x,y)) {
            currentPlayer().errorMsg << inputString << " is already nuked, try another target." << endl;
            return;
        }

		dummyInput = inputString;
		theBoard->nukeAt(x,y);
    }
    else
    if (inputStringLength == 3 && inputString[2] == '!') { // input is valid, attempt spy
         if (currentPlayer().getSpies() == 0) {
            currentPlayer().errorMsg << "No spies available." << endl;
            return;
        }

		dummyInput = inputString;
        theBoard->setSpyRegion(x,y);
		currentPlayer().useSpy();
    }
    else
    if (inputStringLength == 3 && inputString[2] == 'd') { // input is valid, attempt dump for debug
        cout << theBoard->dumpAt(x,y);
        cin.get();
        return;
    } else {
        setInvalidCoordCmd(true);
        return;
    }
    // made it this far, a valid command was entered
    setWarInputValid(true);
}

void Game::handleQuitCmd()
{
    cout << "Exit to home screen? ";
    string reallyQuit;

    Console::getValue(reallyQuit);
    Console::strToLower(reallyQuit);

    if (reallyQuit == "y" || reallyQuit == "yes") {
        setUserAbort(true);
        setGameOver(true);
    }
    else
        return;

    // a valid command
    setWarInputValid(true);
}

void Game::handleHelpCmd()
{
    cout << endl
         << "Thermonuclear War Commands" << endl
         << endl
         << "XY: nuke the designated co-ordinates, eg. B1" << endl
         << "XY!: spy region at the designated co-ordinates eg. B1!" << endl
         << endl
         << "help: display this screen" << endl
         << "quit: back to menu screen" << endl
		 <<	endl
		 << "Press <enter> to continue" << endl;
    cin.get();
}

void Game::handleWarInput()
{
	// human input handling routine

    setInvalidCoordCmd(false);

    inputString = Console::getLine();
    Console::strToLower(inputString);
    inputStringLength = inputString.length();

    if (inputStringLength == 0)
        return;
    // input should only be between 1 and 4 characters
    if (inputStringLength > 4)
        setInvalidCoordCmd(true);
    else
    if (inputString == "help" || inputString == "?")
        handleHelpCmd();
    else
    if (inputString == "quit" || inputString == "exit" || inputString == "q")
        handleQuitCmd();
    else
	if (inputStringLength >= 2)
        handleCoordCmd();

    if (isInvalidCoordCmd())
        currentPlayer().errorMsg << "Please enter 'XY' or 'XY!' ('XY' is co-ord eg. B1), type 'help' for all options" << endl;
}

void Game::displayScores()
{
	Console::clear();
	Console::simulateTyping("\n\nGAME OVER\n");
    setCurrentPlayerToFirst();
    cout << endl << "Scores" << endl << endl;

    do {
        cout << "-----";
        Console::simulateTyping(currentPlayer().getName());
        cout << "-----\n";

        cout << currentPlayer().displayDestroyedCities() << endl;
        string casStr = "Casualties Inflicted: " + Console::toString(currentPlayer().getTotalEnemyCasualties()) + "\n\n";
        Console::simulateTyping(casStr);
    } while (!passToNextPlayer());

	string resultStr;

	if (currentPlayer().getTotalEnemyCasualties() == nextPlayerPtr()->getTotalEnemyCasualties())
		resultStr = "DRAW GAME";
	else {
		if (currentPlayer().getTotalEnemyCasualties() > nextPlayerPtr()->getTotalEnemyCasualties())
			resultStr = currentPlayer().getName();
		else
			resultStr = nextPlayerPtr()->getName();
		resultStr += " WINS";
	}
	Console::simulateTyping(resultStr + "\n\n");
	cout << "Press <enter> to continue." << endl;
    cin.get();
    Console::clear();
}

void Game::preTurnUpdate()	// things to do before a turn is taken
{
	if (currentPlayer().getTurn() == 1) {
		currentPlayer().alertMsg += currentPlayer().getName() + " get ready!\t Fire at target co-ordinates between " 
			+ Console::toString(char(getCurrentPlayerLowX() + 'A')) + "0 - " 
			+ Console::toString(char(getCurrentPlayerHighX() + 'A')) + Console::toString(theBoard->getHeight() - 1) + "!\t\t\n";	 	
	}

	dummyInput = "";
    // WISH change currentPlayer to reference
    if (currentPlayer().isPrevTurnSpy()) {
        theBoard->clearSpyRegion();
    }

	if (currentPlayer().getnPlayersIn()) {	// count players still in
		while (currentPlayer().isOut()) {	// for multiplayer games, loop to next available player still in
//			cout << "In sus loop....\n"; cout.flush();
			passToNextPlayer();
		}
	}

	setWarInputValid(false);
}

void Game::postTurnUpdate()	// things to do after a turn is taken
{
/*
	if (!isGameOver()) {
		updateScreenWithInput(); // TODO
		Console::delayms(1000);
	}
*/	
	currentPlayer().incrementTurn();

	if (postTurnMsg.length())	// print msg only if it's there
		Console::simulateTyping("\n" + postTurnMsg + "\t");
	postTurnMsg = "";
	cout.flush();

    // game over if everyone is out
    if (currentPlayer().getnPlayersIn() == 0)
        setGameOver(true);

    passToNextPlayer();
}

void Game::createTwoPlayers()
{
    string creationStr = "hc";  //default human vs CPU
	cout << endl;
	cout << "  Game type" << endl << endl
         << "    (1) Player vs CPU (default)" << endl
         << "    (2) Player vs Player" << endl
         << "    (3) CPU vs CPU - Demo mode" << endl << endl
         << "> ";

    unsigned selection;
    Console::getValue(selection);

    if (selection == 1) creationStr = "hc";
    if (selection == 2) creationStr = "hh";
    if (selection == 3) creationStr = "cc";

    // control creation of human or CPU player using preset menu string
	// eg. the default "hc" will generate human then cpu
    for (unsigned i = 0; i < kNumPlayers; i++) {
        if (creationStr[i] == 'h')
            players[i] = new HumanPlayer(*this);
        else {
            players[i] = new CpuPlayer(*this);
        }

		// initialise player's values to the game values
        players[i]->setMissilesRemaining(getCitiesPerPlayer() + kNumMissilesAdditionalToCities);
        players[i]->setHomeCitiesRemaining(getCitiesPerPlayer());
        players[i]->setEnemyCitiesRemaining(getCitiesPerPlayer());
    }
}

void Game::setRandomStartingPlayer()
{
    // flip an n sided coin to randomly determine starting player
    int coin = Random::generateRandomInt(1,kNumPlayers);
    while (coin) {
        passToNextPlayer();
        --coin;
    }
}
void Game::init()
{
    setGameOver(true);
    setUserAbort(false);

	generateCityPopulations();

    promptGameDetails();
    createGameBoard();
    createTwoPlayers();
    populateBoard();

    setRandomStartingPlayer();
    
	setGameOver(false);
	firstTurn = true;
}

void Game::cleanup()
{
    currentPlayer().setnPlayersIn(0);   // ensure static nPlayersIn gets reset to 0
 
	// free up the player memory
	for (unsigned i = 0; i < Game::kNumPlayers; i++)
        delete players[i];

	// free up board memory
    delete theBoard;
}

void Game::updateScreenWithAlerts()
{
    drawBoard();

	cout << currentPlayer().errorMsg.str();
    currentPlayer().errorMsg.str("");

    Console::simulateTyping(currentPlayer().alertMsg);
    currentPlayer().alertMsg = "";

	cout << playerPrompt();
}

void Game::updateScreenWithInput()
{
    drawBoard();
    cout << playerPrompt() << dummyInput << endl;
}

string Game::playerPrompt()
{
	stringstream prompt;
	prompt << "<" << setfill('0') << setw(3) << currentPlayer().getTurn() << ' '
		   << currentPlayer().getName() << (currentPlayer().getSpies() ? "!>" : ">") << " ";
	return prompt.str();
}

void Game::run()
{
	// Main game loop
	while (!isGameOver())
    {
        preTurnUpdate();

        while (!isWarInputValid()) { // current Player control until input good
            updateScreenWithAlerts();
            currentPlayer().takeTurn();
        }

        postTurnUpdate();
    }

	// Game over
	if (!isUserAbort()) {	// if user request quit skip scores
        displayScores();
	}
}
