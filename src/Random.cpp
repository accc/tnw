#include "Random.h"

namespace Random
{
    void init()
    {
        srand((unsigned int) time(NULL));
    }

    int generateRandomInt(int inRandMin, int inRandMax)
    {
        return inRandMin + rand() % (inRandMax - inRandMin + 1);
    }
}
