/*

	Player.cpp
	Player class definition

	Alexi Chiotis

*/

#include "Player.h"
#include "Console.h"
#include "Game.h"
#include "Random.h"
#include "time.h"
#include <algorithm>
#include <iomanip>
using namespace std;

// Counters used for naming suffix
unsigned Player::nCpuPlayers = 0;
unsigned Player::nHumanPlayers = 0;
unsigned Player::nPlayersIn = 0;

Player::Player(Game &inGame) : theGame(inGame)
{
    turn = 1;
    out = false;
    spies = 0;
    prevTurnSpy = false;
    enemyCasualtyTally = 0;
    nPlayersIn++;	// add to total players in count
    reportedOut = false;
}

Player::~Player() {}

void Player::setName(std::string inName)
{
    name = inName;
}

unsigned Player::getTurn()
{
    return turn;
}

void Player::incrementTurn()
{
    turn++;
    // gain a spy every k turns
    if ((getTurn() % theGame.kTurnsPerSpyMode) == 0) {
        gainSpy();
    }
}

unsigned Player::getMissilesRemaining()
{
    return missilesRemaining;
}

void Player::setMissilesRemaining(unsigned inMissilesRemaining)
{
    missilesRemaining = inMissilesRemaining;
}

void Player::decMissiles()
{
    if (getMissilesRemaining())	{	// avoid wrapping int
        missilesRemaining--;
        if (getMissilesRemaining() == 1)
            alertMsg += "CRITICAL! One missile remaining.\n";
    }
    if (getMissilesRemaining() == 0) {
        theGame.postTurnMsg = getName() + " is out, missile stockpile depleted.\n";
		setOut();
	}
}

void Player::decHomeCitiesRemaining()
{
    if (getHomeCitiesRemaining())	// avoid wrapping int 
		homeCitiesRemaining--;
    
	if (getHomeCitiesRemaining() == 1)
        alertMsg += "CRITICAL! One home city remaining.\n";

    if (getHomeCitiesRemaining() == 0) {
        theGame.postTurnMsg = getName() + "'s cities are all destroyed!\n";
		setOut();
	}
}

void Player::decEnemyCitiesRemaining()
{
	if (getEnemyCitiesRemaining())	// avoud wrapping int
		enemyCitiesRemaining--;

    if (getEnemyCitiesRemaining() == 0)
		setOut();
}

void Player::setHomeCitiesRemaining(unsigned inCities)
{
    homeCitiesRemaining = inCities;
}

void Player::setEnemyCitiesRemaining(unsigned inCities)
{
    enemyCitiesRemaining = inCities;
}

unsigned Player::getEnemyCitiesRemaining()
{
    return enemyCitiesRemaining;
}

unsigned Player::getHomeCitiesRemaining()
{
    return homeCitiesRemaining;
}

void Player::addToEnemyCasualties(unsigned long inCasualties)
{
    enemyCasualtyTally += inCasualties;
}

unsigned long Player::getTotalEnemyCasualties() const
{
    return enemyCasualtyTally;
}

void Player::addCityToDestroyedList(City &inCity)
{
    destroyedCities.push_back(inCity);
}


unsigned Player::getnPlayersIn()
{
    return nPlayersIn;
}

void Player::setnPlayersIn(unsigned innPlayersIn)
{
    nPlayersIn = innPlayersIn;
}

std::string Player::stats()
{
    stringstream stats;
    stats << getName() << " " << (isOut() ? "Out" : "In ") << " T:" << getTurn()
          << " E:" << theGame.getCitiesPerPlayer() - getEnemyCitiesRemaining()
          << " M:" << getMissilesRemaining() << " S:" << getSpies();
    return stats.str();
}

std::string Player::displayDestroyedCities()
{
    stringstream score;
	unsigned enemyCasualtyTally = 0;

	// go through the cities and output the city info
	for (std::vector<City>::iterator it = destroyedCities.begin(); it != destroyedCities.end(); ++it) {
        score << it->stats() << endl;
        enemyCasualtyTally += it->getPopulation();
    }
    return score.str();
}

void Player::decnPlayersIn()
{
	if (Player::nPlayersIn != 0)
		--Player::nPlayersIn;
}

void Player::setOut()
{
		out = true;	// value will wrap
		decnPlayersIn();
}

bool Player::isOut()
{
	return out;
}

unsigned Player::getSpies()
{
    return spies;
}

void Player::useSpy()
{
    spies--;
    prevTurnSpy = true;
}

void Player::resetSpy()
{
    prevTurnSpy = false;
}

bool Player::isPrevTurnSpy()
{
    return prevTurnSpy;
}

void Player::gainSpy()
{
    spies++;
}

bool Player::isReportedOut()
{
    return reportedOut;
}

void Player::setReportedOut(bool inState)
{
    reportedOut = inState;
}
void HumanPlayer::gainSpy()
{
    Player::gainSpy();
    alertMsg += getName() + " gained a spy! Enter co-ordinates followed by '!' to deploy\n";
}

std::string Player::getName()
{
    return name;
}

HumanPlayer::HumanPlayer(Game &inGame) : Player(inGame)
{
    ++Player::nHumanPlayers;
    string newName = "Player " + Console::toString(Player::nHumanPlayers);
    setName(newName);
}

HumanPlayer::~HumanPlayer()
{
    --Player::nHumanPlayers;
}

void HumanPlayer::takeTurn()
{
    theGame.handleWarInput();
}

CpuPlayer::CpuPlayer(Game &inGame) : Player(inGame)
{
    ++Player::nCpuPlayers;
    string newName = "CPU " + Console::toString(Player::nCpuPlayers);
    setName(newName);
}

CpuPlayer::~CpuPlayer()
{
    --Player::nCpuPlayers;
}

void CpuPlayer::takeTurn()
{
    // WISH spy for CPU, spy and remember revealed city to nuke
    unsigned x, y;

    do {	// select a random spot within the player's space that is untouched
        x = Random::generateRandomInt(theGame.getCurrentPlayerLowX() , theGame.getCurrentPlayerHighX());
        y = Random::generateRandomInt(0, theGame.theBoard->getHeight() - 1);
    } while (theGame.theBoard->isNukedAt(x,y));

    // CPU types at the prompt...
	theGame.dummyInput = Console::toString(static_cast<char>('a'+ x)) + Console::toString(y);// + '\n';
    Console::simulateTyping("\t" + theGame.dummyInput);
   
	// And nukes.
    theGame.theBoard->nukeAt(x,y);	
	theGame.setWarInputValid(true);
}
