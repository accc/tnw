/*

	Console.h
	Console namespace

	Alexi Chiotis 24/04/2012

	Console routines and some type conversion routines that don't go anywhere else

*/

#ifndef CONSOLE_H
    #define CONSOLE_H

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <time.h>
#include "Random.h"

namespace Console
{
    std::string getLine();	// a solid routine for handling user input
    void simulateTyping(const std::string &str);

    void delayms(unsigned delay);
    void clear();

    // definition must be in header file otherwise not defined error will occur
    template <typename T>
    void getValue(T &value)
    {
        std::string inputStr = Console::getLine();
        std::istringstream strStream(inputStr);

        //WISH: Error checking for this
        strStream >> value;
    }

    template <typename T>
    std::string toString (const T& t)
    {
        std::stringstream ss;
        ss << t;
        return ss.str();
    }

    template <typename T>
    T strTo(const std::string& str)
    {
        T t;
        std::istringstream ss(str);
        ss >> t;
        T result;
        //WISH Error checking for this
        return t;
    }

	void strToLower(std::string &inStr);
}

#endif
