/*

    App.h
    App class definition

	Alexi Chiotis

*/
#ifndef APP_H
    #define APP_H

#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <fstream> // for createCityListFromFile
#include "City.h"


class App {

protected:

    int argc;
    char **argv;

	bool initialOutput;

	static const char *kCityFilename;
    void createCityListFromFile();

public:
    App();
    void dumpState();
	void displayTitle();
    void displayHomeScreen();
    void displayInstructions();
    void editGameDetails();
    void setArgs(int argc, char **argv);
	void toggleTestMode();

    void init();
    void run();

    unsigned boardWidth,
            boardHeight,
            nCities;

    std::vector<City> cities;

    bool isDone;
	bool testMode;
};

#endif
