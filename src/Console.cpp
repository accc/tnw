/*

    Console.cpp
    Console namespace implementation

	Miscellaneous functions for handling input and output

	Alexi Chiotis

*/

#include "Console.h"

#ifdef _WIN32
#include <windows.h> //Win32
#endif

using namespace std;

namespace Console
{
    string getLine()
    {
        string str;
        while (true)
        {
            cin.clear();
            if (cin.eof()) {
                break;  // handle eof (Ctrl-D) gracefully
            }

            if (cin.good()) {
                char next = cin.get();
                if (next == '\n')
                    break;
                str += next;    // add character to string
            } else {
                cin.clear(); // clear error state
                string badToken;
                cin >> badToken;
                cerr << "Bad input encountered: " << badToken << endl;
            }
        }
        return str;
    }

    void delayms(unsigned time)
    {
        #ifdef _WIN32
		Sleep(time);	// Win32
		#else
        usleep(time*1000);
        #endif
    }

    void simulateTyping(const string &str)
    {
        cout.flush();
        stringstream ss(str);
        char next, prev = '\0';
        for (unsigned i = 0; i < str.length(); i++) {
            unsigned sameKeyDelay = 40;

            ss.get(next);
            // treat tabs as pauses
            if (next == '\t')
                delayms(Random::generateRandomInt(400, 600));
            else if (next == '\b')
                cout << '\b' << ' ' << '\b';
            else
                cout << next;

            cout.flush();
            if (prev == next)
                delayms(sameKeyDelay);
            else
                delayms(Random::generateRandomInt(15, 120));

            //prePrev = prev;
            prev = next;
        }
    }

    void strToLower(string &inStr)
    {
        std::transform(inStr.begin(), inStr.end(), inStr.begin(), ::tolower);
    }

    void clear()
    {
        #ifdef _WIN32
		system("cls");	//Win32
		#else
		system("clear");
        #endif
    }
}
